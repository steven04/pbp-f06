// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';


function loadJson(selector) {
  return JSON.parse(document.querySelector(selector).getAttribute('data-json'));
}

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

// Pie Chart Example
var vaksin = loadJson("#myChart");
var total = 277196503;
var vaksin1 = (vaksin[0].jumlah_vaksinasi_1 / total * 100).toFixed(2);
var vaksin2 = (vaksin[0].jumlah_vaksinasi_2 / total * 100).toFixed(2);
var belumVaksin = 100 - vaksin1 - vaksin2;

var ctx = document.getElementById("myChart");
var myPieChart = new Chart(ctx, {
  type: 'doughnut',
  data: {
    labels: ["Belum Divaksin", "Vaksin 1", "Vaksin 2"],
    datasets: [{
      data: [belumVaksin, vaksin1, vaksin2],
      backgroundColor: ['#f6c23e', '#36b9cc', '#1cc88a'],
      hoverBackgroundColor: ['#dba448', '#2c9faf', '#17a673'],
      hoverBorderColor: "rgba(234, 236, 244, 1)",
    }],
  },
  options: {
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
    },
    legend: {
      display: false
    },
    cutoutPercentage: 80,
  },
});
