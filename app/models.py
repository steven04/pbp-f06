from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Profile(models.Model):
    user = models.OneToOneField(
        User, null=True, blank=True, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=200, null=True)
    last_name = models.CharField(max_length=200, null=True)
    gender = models.CharField(max_length=200, null=True)
    number_phone = models.CharField(max_length=200, null=True)
    bio = models.CharField(max_length=200, null=True)
    province = models.CharField(max_length=200, null=True)
    date_of_birth = models.DateField(null=True)
    profile_pic = models.ImageField(
        default="undraw_profile.svg", upload_to='users/', null=True, blank=True)

    def __str__(self):
        return f'{self.user.username}-Profile'


class Question(models.Model):
    username = models.CharField(max_length=100)
    question = models.TextField()
    date = models.DateTimeField(auto_now_add=True, blank=True)
