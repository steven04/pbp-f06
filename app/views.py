from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Profile, Question
from django.http import JsonResponse
from django.contrib.auth.models import Group
from .forms import NewUserForm, UserUpdateForm, ProfileUpdateForm, QuestionForm
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
import requests
import json

# Create your views here.


def error_404(request, exception):
    context = {'title': 'Halaman Tidak Ditemukan'}
    return render(request, '404.html', context)


def index(request):
    context = {'title': 'Dashboard'}
    if request.user.is_authenticated:
        context['username'] = request.user.username
    response = requests.get(
        'https://data.covid19.go.id/public/api/update.json').json()
    context['harian'] = json.dumps([response['update']['harian']])
    total = response['update']['total']
    context['created'] = response['update']['penambahan']['created']
    context['positif'] = "{:,}".format(
        int(total['jumlah_positif'])).replace(',', '.')
    context['dirawat'] = "{:,}".format(
        int(total['jumlah_dirawat'])).replace(',', '.')
    context['sembuh'] = "{:,}".format(
        int(total['jumlah_sembuh'])).replace(',', '.')
    context['meninggal'] = "{:,}".format(
        int(total['jumlah_meninggal'])).replace(',', '.')
    response = requests.get(
        'https://data.covid19.go.id/public/api/pemeriksaan-vaksinasi.json').json()
    context['vaksin'] = json.dumps(
        [
            response['vaksinasi']['total']
        ])
    return render(request, 'index.html', context)


def login_request(request):
    context = {'title': 'Login'}
    if request.user.is_authenticated:
        return redirect("/")
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            remember = request.POST.get('remember')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                if remember == "on":
                    request.session.set_expiry(60*60*24*30)
                else:
                    request.session.set_expiry(0)
                messages.success(request, "Login berhasil dilakukan.")
                return redirect("/")
        else:
            messages.error(
                request, "Username atau password salah.")
    return render(request, "login.html", context)


def register_request(request):
    context = {'title': 'Register'}
    if request.user.is_authenticated:
        return redirect("/")
    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            group = Group.objects.get(name='New_User')
            user.groups.add(group)
            login(request, user)
            messages.success(request, "Registrasi Berhasil.")
            return redirect("/setting")
        else:
            messages.error(
                request, "Registrasi gagal. Informasi tidak valid.")
    return render(request, "register.html", context)


def logout_request(request):
    logout(request)
    messages.info(request, "Logout berhasil dilakukan.")
    return redirect("/")


def provinsi(request):
    context = {'title': 'Data Covid-19 per Provinsi'}
    if request.user.is_authenticated:
        context['username'] = request.user.username
    response = requests.get(
        'https://data.covid19.go.id/public/api/prov.json').json()
    for data in response['list_data']:
        data['jumlah_dirawat'] = "{:,}".format(
            int(data['jumlah_dirawat'])).replace(',', '.')
        data['jumlah_sembuh'] = "{:,}".format(
            int(data['jumlah_sembuh'])).replace(',', '.')
        data['jumlah_meninggal'] = "{:,}".format(
            int(data['jumlah_meninggal'])).replace(',', '.')
    context['response'] = response
    response = requests.get(
        'https://data.covid19.go.id/public/api/update.json').json()
    context['created'] = response['update']['penambahan']['created']
    return render(request, 'provinsi.html', context)


def berita(request):
    context = {}
    return render(request, 'berita.html', context)


def forum(request):
    context = {}
    return render(request, 'forum.html', context)


@login_required(login_url='/login')
def profile(request):
    context = {'title': 'User Profile'}
    if request.user.is_authenticated:
        context['username'] = request.user.username
    return render(request, 'profil.html', context)


@login_required(login_url='/login')
def setting(request):
    context={}
    response = requests.get(
        'https://dev.farizdotid.com/api/daerahindonesia/provinsi').json()
    for data in response['provinsi']:
        data['nama'] = str(data['nama'])

    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(
            request.POST, request.FILES, instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            return redirect("/profil")

    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context["title"] = "User Settings"
    context["response"] = response
    context["username"] = request.user.username
    context["u_form"] = u_form
    context["p_form"] = p_form
    return render(request, 'settings.html', context)


def change_password(request):
    context = {'title': 'Security'}
    if request.user.is_authenticated:
        context['username'] = request.user.username
    ch = Profile.objects.filter(user__id=request.user.id)
    if len(ch) > 0:
        data = Profile.objects.get(user__id=request.user.id)
        context["data"] = data
    if request.method == "POST":
        current = request.POST["cpwd"]
        new_pas = request.POST["npwd"]

        user = User.objects.get(id=request.user.id)
        un = user.username
        check = user.check_password(current)

        if check == True and current == new_pas:
            context["msz"] = "Password yang ingin diubah sama. Mohon periksa kembali."
            context["col"] = "alert-danger"
            user = User.objects.get(username=un)
            login(request, user)
        
        else:
            if check == True:
                user.set_password(new_pas)
                user.save()
                context["msz"] = "Berhasil mengganti password."
                context["col"] = "alert-success"
                user = User.objects.get(username=un)
                login(request, user)
            else:
                context["msz"] = "Password yang dimasukkan salah."
                context["col"] = "alert-danger"
    return render(request, "change_password.html", context)


def addQuestion(request):
    if request.method == "POST":
        form = QuestionForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Pertanyaan berhasil disampaikan.")
        else:
            messages.error(request, "Pertanyaan gagal disampaikan.")
    return redirect("/")


def delete_user(request):    
    user = request.user
    user.delete()
    messages.success(request, "Berhasil menghapus akun.")    
    logout(request)        
    return redirect("/")

def rumahsakit(request):
    context = {}
    return render(request, 'rumah_sakit.html', context)