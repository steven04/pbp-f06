# PBP-F06



## Anggota Kelompok

- 2006595993 - Adeline Sonia Sanusie
- 2006597052 -  Ainun Nur Rohmah
- 2006596301 - Keyza Asyadda Ramadhan M.
- 2006596586 - Nathanael Horasi Ondiraja
- 2006597260 - Raissa Adlina Febrianny
- 2006596913 - Steven
- 2006596812 - Sulthan Muhammad Fasda 

## Link Herokuapp

https://pbp-f06.herokuapp.com/

## Cerita Aplikasi yang Diajukan serta Kebermanfaatannya

#### Deskripsi Singkat

Aplikasi yang akan dibuat adalah aplikasi yang menampilkan informasi-informasi mengenai covid-19 seperti data sebaran dan artikel serta berita terkini. Selain bisa mendapatkan informasi terbaru mengenai covid-19 dengan adanya aplikasi ini pengguna juga dapat terhindarkan dari berbagai berita hoax mengenai Covid-19. Pengguna dapat melakukan diskusi dengan pengguna lain mengenai covid-19. 

#### Manfaat
- Memberitahukan informasi covid-19 terkini dan valid kepada pengguna
- Ada forum diskusi untuk membahas hal-hal yang berkaitan dengan covid-19


## Daftar Modul yang akan Diimplementasikan

- Menampilkan data covid-19 Indonesia dari API
- Fitur login dan logout
- User registration
- Forum untuk membahas covid-19 dan lain-lain (login required)
- Halaman berisi artikel/berita terkini tentang covid
- User dapat mengganti profil dan deskripsi diri
- User dapat mengganti password (kode verifikasi dikirimkan ke email user)

## User Persona

#### Profil 1

**Deskripsi:** Seorang mahasiswa yang baru saja pindah ke kota baru. Ia belum memiliki tempat tinggal yang tetap sehingga sering berada di luar. Hal ini membuatnya lebih terekspos terhadap virus covid-19, khususnya di daerah perkotaan Jabodetabek.

**Goals:** Mendapatkan informasi terkini covid-19 di kota ia berada

**Needs:** Berita covid-19 terkini, jumlah kasus positif lokal, situasi vaksin di Indonesia

**Frustration:** Sering menemukan berita covid-19 hoax yang bersebaran

#### Profil 2

**Deskripsi:** Seorang pekerja kantoran yang sering ditugaskan untuk dinas ke luar kota sehingga ia tahu keadaan dari kota-kota yang pernah dikunjungi. Selain itu, ia perlu untuk mencari tahu keadaan COVID-19 dari kota yang akan dituju melalui data-data dan berita yang ada.

**Goals:** Bisa mengetahui keadaan COVID-19 dari kota yang akan dikunjungi, memberikan informasi terkait kota-kota yang pernah ia kunjungi melalui forum, tidak merasa khawatir apabila ingin bepergian ke kota tujuan.

**Needs:** Jumlah kasus positif dari kota tujuan, informasi-informasi aktual terkait COVID-19 dari kota yang ingin dikunjungi, platform untuk berbagi informasi mengenai keadaan COVID-19 dari kota yang pernah dikunjungi.

**Frustration:** Sulitnya mendapatkan berita mengenai covid-19 yang valid, pernah menemukan berita hoax terkait COVID-19 sehingga ia membatalkan perjalanan dinasnya.

#### Profil 3

**Deskripsi:** Seorang ibu rumah tangga yang sering mendapatkan hoaks dan percaya terhadap konspirasi-konspirasi mengenai Covid-19 yang membutuhkan informasi terkini dan valid. 

**Goals:** Bisa mengetahui mengenai informasi terkini dan valid mengenai covid-19 dan berdiskusi untuk memperoleh pengetahuan dan pendapat pengguna lain.

**Needs:** Berita/informasi terkini dan valid mengenai covid-19

**Frustration:** Sering menemui informasi terkait covid-19 yang tidak kredibel

#### Profil 4

**Deskripsi:** Seorang pria yang berprofesi sebagai pilot dan melakukan penerbangan internasional dengan penumpang dari berbagai negara

**Goals:** Mampu membuat keadaan pesawat tetap dalam keadaan steril dan menjaga kesehatan antar awak pesawat maupun penumpang

**Needs:** Informasi mengenai positif atau negatifnya penumpang yang menjalankan penerbangan internasional

**Frustration:** Sering terdapat penumpang yang cenderung melepaskan maskernya dan tidak patuh terhadap prosedur kesehatan Covid-19